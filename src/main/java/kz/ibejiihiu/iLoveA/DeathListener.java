package kz.ibejiihiu.iLoveA;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DeathListener implements Listener {
    private final File configFile;
    private FileConfiguration config;

    public DeathListener(File configFile) {
        this.configFile = configFile;

        // Загрузка конфигурации из файла
        reloadConfig();
    }

    private void reloadConfig() {
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        // Проверяем, был ли игрок убит другим игроком
        if (player.getKiller() != null) {
            Player killer = player.getKiller();
            String killerName = killer.getName();
            String victimName = player.getName();

            ConfigurationSection commandsSection = config.getConfigurationSection("commands");
            if (commandsSection != null) {
                for (String key : commandsSection.getKeys(false)) {
                    List<String> commands = commandsSection.getStringList(key);

                    for (String command : commands) {
                        // Замена *killer* на ник убийцы
                        command = command.replace("*killer*", killerName);

                        // Замена *murder* на ник жертвы
                        command = command.replace("*murder*", victimName);

                        // Выполнение команды
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                    }
                }
            }
        }
    }

    public void saveConfig() {
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
