package kz.ibejiihiu.iLoveA;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public final class Main extends JavaPlugin {
    private DeathListener deathListener;

    @Override
    public void onEnable() {
        // Создание/получение файла конфигурации
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            saveResource("config.yml", false);
        }

        // Генерация конфигурации, если файл не существует или пустой
        if (configFile.length() == 0) {
            ConfigGenerator.generateConfig(configFile);
        }

        // Создание и регистрация слушателя событий
        deathListener = new DeathListener(configFile);
        getServer().getPluginManager().registerEvents(deathListener, this);
    }

    @Override
    public void onDisable() {
        // Сохранение конфигурации перед выключением плагина
        deathListener.saveConfig();
    }
}
