package kz.ibejiihiu.iLoveA;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ConfigGenerator {

    public static void generateConfig(File configFile) {
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        // Генерация примеров команд
        config.set("commands", Arrays.asList(
                "say *killer* killed *murder*",
                "say *killer* 1 *murder*"
        ));

        // Сохранение конфигурации в файл
        try {
            config.save(configFile);
            System.out.println("Config generated successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
